﻿using Microsoft.Owin;
using Owin.Demo.Middleware.Logging;

[assembly: OwinStartup("DemoConfiguration", typeof(Owin.Demo.Configuration.Startup))]

namespace Owin.Demo.Configuration
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            //app.Run(ctx =>
            //{
            //    ctx.Response.ContentType = "text/plain";
            //    return ctx.Response.WriteAsync("Hello World!");
            //});
            app.Use(typeof (LoggingMiddleware));
            Owin.Demo.WebApi.Config.UseWebApi(app);
        }
    }
}
