To run the application, follow the steps:

1) Run "npm install" command to install all the packages in "package.json" file.

2) Run "node server.js" command to run the server. 
Note: If you are not using the local copy of node in the folder. Please copy "node.exe.config" to the folder where node.exe is located. This is required for the assembly redirecting to owrk correctly.

3) In your browser go to "http://localhost:3000/api/countries".