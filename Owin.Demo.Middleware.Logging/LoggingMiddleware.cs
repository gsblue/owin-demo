﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace Owin.Demo.Middleware.Logging
{
    public class LoggingMiddleware: OwinMiddleware
    {
        public LoggingMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public async override Task Invoke(IOwinContext context)
        {
            var startTime = DateTime.Now;
            Debug.WriteLine("Begin Request");
            await Next.Invoke(context);
            Debug.WriteLine("End Request: {0}", DateTime.Now.Subtract(startTime));
        }
    }
}
