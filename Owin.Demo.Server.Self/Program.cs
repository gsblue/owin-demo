﻿using System;
using Microsoft.Owin.Hosting;
using Owin.Demo.Configuration;

namespace Owin.Demo.Host.Self
{
    class Program
    {
        static void Main(string[] args)
        {
            const string baseUrl = "http://localhost:5000/";
            
            using (WebApp.Start<Startup>(baseUrl))
            {
                Console.WriteLine("Press Enter to quit.");
                Console.ReadKey();
            }
        }
    }
}
