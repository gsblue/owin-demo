﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Owin.Demo.WebApi.Model;

namespace Owin.Demo.WebApi
{
    public class CountriesController: ApiController
    {
        // GET api/values 
        public IEnumerable<Country> Get()
        {
            return DummyDataContext.GetCountries();
        }

        // GET api/values/5 
        public Country Get(int id)
        {
            return DummyDataContext.GetCountries().First(item => item.Id == id);
        } 

    }
}
