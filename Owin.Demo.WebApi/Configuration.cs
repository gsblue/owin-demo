﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin;

namespace Owin.Demo.WebApi
{
    public class Config
    {
        public static IAppBuilder UseWebApi(IAppBuilder appBuilder)
        {

            // This is required when not using Microsoft.Owin's AppBuilder
            // http://katanaproject.codeplex.com/workitem/81
            Microsoft.Owin.Infrastructure.SignatureConversions.AddConversions(appBuilder);

            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "StatesApi",
                routeTemplate: "api/countries/{countryId}/states/{id}",
                defaults: new { controller="states", id = RouteParameter.Optional }
            );

            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            
           
            return appBuilder.UseWebApi(config); 
        }
    }
}
