﻿using System.Collections.Generic;

namespace Owin.Demo.WebApi.Model
{
    
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
        public string Population { get; set; }
    }

    public class State
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public string Population { get; set; }
        public bool IsCapital { get; set; }
    }


    public static class DummyDataContext
    {
        public static IEnumerable<Country> GetCountries()
        {
            return new[]
            {
                new Country()
                {
                    Id = 1,
                    Name = "Australia",
                    Currency = "AUD",
                    Population = "22.68 Million"
                },
                new Country()
                {
                    Id = 2,
                    Name = "India",
                    Currency = "Rupees",
                    Population = "1.237 Billion"
                }
            };
        }

        public static IEnumerable<State> GetStates()
        {
            return new[]
            {
                new State()
                {
                    CountryId = 1,
                    Id = 1,
                    Name = "Victoria",
                    Population = "5.713 Million"
                },
                new State()
                {
                    CountryId = 1,
                    Id = 2,
                    Name = "New South Wales",
                    Population = "7.273 Million"
                },
                new State()
                {
                    CountryId = 1,
                    Id = 3,
                    Name = "Canberra",
                    Population = "0.373 Million",
                    IsCapital = true
                },
                new State()
                {
                    CountryId = 2,
                    Id = 4,
                    Name = "Maharashtra",
                    Population = "115.97 Million"
                },
                new State()
                {
                    CountryId = 2,
                    Id = 5,
                    Name = "Delhi",
                    Population = "16.79 Million",
                    IsCapital = true
                },
                new State()
                {
                    CountryId = 2,
                    Id = 6,
                    Name = "Gujarat",
                    Population = "60.44 Million"
                },
           
            };
        }
    }
}
