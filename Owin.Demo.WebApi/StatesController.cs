﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Owin.Demo.WebApi.Model;

namespace Owin.Demo.WebApi
{
    public class StatesController : ApiController
    {
        public IEnumerable<State> Get(int countryId)
        {
            return DummyDataContext.GetStates().Where(item => item.CountryId == countryId);
        }

    }
}
